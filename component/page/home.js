import React from 'react';
import {
    Text, View, StyleSheet, ActivityIndicator,
    Image, List, ScrollView, Dimensions, TouchableOpacity
} from 'react-native';
import Axios from 'axios';
import CustomCard from '../components/card.js';
import { connect } from 'react-redux'
import { types, screen } from '../redux/types'
class Home extends React.Component {

    constructor() {
        super()
    }
    componentWillMount() {
        this.getIndonesia();
    }

    getIndonesia = async () => {
        try {
            this.props.setLoading(true)
            this.props.setError(false)
            const response = await Axios.get(`https://api.kawalcorona.com/indonesia`, {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Authorization, X-Request-With'
                }
            })
            this.props.setData(response.data)
        } catch (error) {
            console.log(error);
            this.props.setLoading(false)
            this.props.setError(true)
        }
    }

    render() {
        const { navigation } = this.props
        
        if (this.props.isLoading) {
            return (
                <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                    <ActivityIndicator size='large' color='red' />
                </View>
            )
        }
        else {
            if (this.props.isError) {
                return (
                    <View
                        style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                    >
                        <Text>Terjadi Error Saat Memuat Data</Text>
                    </View>
                )
            }
            else {
                if(this.props.data===undefined){
                    return(<View></View>)
                }
                let indonesia = this.props.data[0];
                return (
                    <View style={{ padding: 8 }}>
                        <ScrollView>
                            <Text style={styles.title}>Indonesia Covid-19 Statistic</Text>
                            <CustomCard props_kasus={{
                                total_kasus: indonesia.positif,
                                warna: '#F6B93B',
                                status: 'POSITIF',
                                icons: require('../../assets/hospital.png')
                            }} />
                            <CustomCard props_kasus={{
                                total_kasus: indonesia.sembuh,
                                warna: '#78E08F',
                                status: 'SEMBUH',
                                icons: require('../../assets/healthy.png')
                            }} />
                            <CustomCard props_kasus={{
                                total_kasus: indonesia.meninggal,
                                warna: '#EB2F06',
                                status: 'MENINGGAL',
                                icons: require('../../assets/dead.png')
                            }} />
                            <TouchableOpacity onPress={() => { navigation.push('Detail') }} style={{ flex: 1, borderRadius: 120, backgroundColor: '#830678', margin: 16, width: '80%', padding: 8, borderRadius: 8, alignSelf: 'center' }}>
                                <Text style={{ color: 'white', alignSelf: 'center', fontWeight: 'bold', fontSize: 16 }}>Data Per Provinsi</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                );
            }
        }
        return(<View></View>)
    }
}

const styles = StyleSheet.create({
    title: {
        marginLeft: 10,
        fontSize: 30,
        alignSelf: 'center',
        marginTop: 50,
        marginBottom: 10,
        fontWeight: 'bold'
    }
});

const mapStateToProps = state => {
    return {
        data: state.home.data,
        isLoading: state.home.isLoading,
        isError: state.home.isLoading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setData: (data) =>
            dispatch({ type: types.UPDATE_DATA, screen: screen.HOME, data }),
        setLoading: (isLoading) =>
            dispatch({ type: types.LOADING, screen: screen.HOME, isLoading }),
        setError: (isError) =>
            dispatch({ type: types.ERROR, screen: screen.HOME, isError }),
    }
}

const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(Home)
export default HomeScreen