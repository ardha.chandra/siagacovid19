import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView } from 'react-native';

export default class About extends React.Component {
    render(){
        return(
            <View style={styles.main_container}>
                <ScrollView>
                    <Text style={{
                        fontSize: 20,
                        alignSelf:'center',
                        fontWeight: '700'
                    }}>Meet the Team</Text>
                    <View style={styles.center_card}>
                        <Image 
                            source={require('../../assets/profil.png')}
                            style={styles.avatar}
                        />
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 20
                        }}>Ketut Ardha Chandra</Text>
                        <Text style={{
                            marginBottom: 5,
                            marginTop: 10,
                            fontWeight: 'bold',
                            fontSize: 15
                        }}>Web Developer</Text>
                        <Text style={{
                            textAlign: 'justify',
                            fontSize: 15
                        }}>Siap belajar dan menguasai hal baru.
                        Menguasai :
                        1. PHP
                        2. Javascript
                        3. CSS
                        4. Codeigniter Framework
                        5. Bootstrap
                        6. Jquery Framework
                        7. On Progress menguasai ReactNative, Laravel dan lainnya.</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        display: 'flex',
        alignItems: 'center',
        padding:8
    },
    center_card: {
        backgroundColor: 'white',
        elevation: 1,
        padding: 20,
        borderRadius: 5,
        borderStyle: 'solid',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    avatar: {
        width: '100%',
        height: 250,
        marginBottom: 20
    }
});