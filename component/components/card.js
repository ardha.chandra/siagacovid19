import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export default class CustomCard extends React.Component {
    render(){
        let prop = this.props.props_kasus;
        return(
            <View style={{
                backgroundColor: prop.warna,
                marginLeft: 10,
                marginRight: 10,
                display: 'flex',
                flexDirection: 'row',
                padding: 20,
                alignItems: 'center',
                borderRadius: 120,
                elevation: 5,
                marginTop: 10,
                marginBottom: 10
            }}>
                <Image source={prop.icons} style={
                    styles.icon
                }/>
                <Text style={
                    styles.status
                }>{prop.status}</Text>
                <View style={{
                    flexDirection: 'column',
                    flexBasis: '30%',
                    marginTop: 18
                }}>
                    <Text style={styles.total}>{prop.total_kasus}</Text>
                    <Text style={{
                        color: 'white'
                    }}>Orang</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    status: {
        flexBasis: '45%',
        color: 'white',
        fontSize: 20,
        fontWeight: '700',
        marginLeft: 15
    },
    icon: {
        flexBasis: '25%'
    },
    total: {
        color: 'white',
        fontSize: 20,
        fontWeight: '700'
    }
});