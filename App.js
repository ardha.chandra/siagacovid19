import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import 'react-native-gesture-handler';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './component/redux/reducers'
import LoginScreen from './component/page/login'
import HomeScreen from './component/page/home.js';
import AboutScreen from './component/page/about_us.js';
import DetailScreen from './component/page/detail.js'
const NavStack = createStackNavigator();
const TabStack = createBottomTabNavigator();

const HomeStack = createStackNavigator()
const HomeStackScreen = () => (
  <HomeStack.Navigator screenOptions={{ headerShown: false }}>
    <HomeStack.Screen name="Home" component={HomeScreen} />
    <HomeStack.Screen name="Detail" component={DetailScreen} />
  </HomeStack.Navigator>
)

const LoginStack = createStackNavigator()
const LoginStackScreen = () => (
  <LoginStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Login">
    <LoginStack.Screen name="Login" component={LoginScreen}></LoginStack.Screen>
    <LoginStack.Screen name="Home" component={BottomStack}></LoginStack.Screen>
  </LoginStack.Navigator>
)
const BottomStack = () => {
  return (
    <TabStack.Navigator initialRouteName="Detail" screenOptions={{ headerShown: false }}>
      <TabStack.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="ios-home" color={color} size={size} />
          )
        }}
      />
      <TabStack.Screen
        name="About"
        component={AboutScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="ios-person" color={color} size={size} />
          )
        }}
      />
    </TabStack.Navigator>
  );
}

export default function App() {
  const store = createStore(reducer)
  return (
    <Provider store={store}>
      <NavigationContainer>
        <LoginStackScreen/>
      </NavigationContainer>
    </Provider>

  );
}
